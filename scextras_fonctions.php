<?php
/**
 * Fonctions utiles au plugin Champs Extras (Synchronisation)
 *
 * @plugin     Champs Extras (Synchronisation)
 * @copyright  2013
 * @author     Bruno Caillard
 * @licence    GNU/GPL
 * @package    SPIP\scextras\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/*
 * Un fichier de fonctions permet de définir des éléments
 * systématiquement chargés lors du calcul des squelettes.
 *
 * Il peut par exemple définir des filtres, critères, balises, …
 *
 */

?>